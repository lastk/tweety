class TweetPresenter < BasePresenter

  def self.present_collection(collection)
    collection.map { |item| TweetPresenter.new(item) }
  end

  def initialize(tweet)
    @tweet = tweet
    __setobj__(@tweet)
  end

  def profile_link
    helpers.link_to("http://twitter.com/#{screen_name}") do
      helpers.image_tag(profile_image)
    end
  end

  def tweet_link
    helpers.link_to(formated_time, "http://twitter.com/#{screen_name}/#{id}")
  end

  private

  def formated_time
    created_at.to_time.to_s(:post)
  end
end
