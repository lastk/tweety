require Rails.root.join  'spec/support/locaweb_tweets'
class TweetsController < ApplicationController
  def index
    # paramets doesnt works with locaweb api
    result = TweetClient.new(LocawebTweets.new).query(params[:q])
    urgents = Tweet::Urgents.new(tweets:result).all.reverse

    @tweets = TweetPresenter.present_collection(urgents)
  end
end
