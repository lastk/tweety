class TweetClient
  def initialize(driver)
    @driver = driver
  end

  def query(by)
    result = @driver.search_tweets(by)
    to_models(result)
  end

  private

  def to_models(result_list)
    tweets = result_list["statuses"]
    models = []
    tweets.each do |t|
      models << Tweet.new(screen_name: t["user"]["screen_name"],
        followers_count: t["user"]["followers_count"],
        likes_count: t["user"]["favourites_count"],
        retweets_count: t["retweet_count"],
        text: t["text"],
        profile_image: t["user"]["profile_image_url_https"],
        created_at: t["created_at"],
        in_reply_to_user_id: t["in_reply_to_user_id"],
        id: t["id_str"]
      )
    end
    models
  end
end
