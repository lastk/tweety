class Tweet
  include ActiveModel::AttributeAssignment
  def initialize(params)
    assign_attributes(params)
  end

  attr_accessor :id, :screen_name, :followers_count, :retweets_count,
                :likes_count,:text, :profile_image, :created_at,
                :in_reply_to_user_id
end
