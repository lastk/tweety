class Tweet::Urgents
  def initialize(filters = {}, tweets:)
    @mention_user_filter = filters.fetch(:mention_user_filter, Tweet::MentionUserFilter.new)
    @exclude_reply_to_filter = filters.fetch(:exclude_reply_to, Tweet::ExcludeReplyToFilter.new)

    @tweets = sorted(tweets)
  end

  def all
    @tweets
  end

  private

  def sorted(tweets)
    return [] if tweets.nil? or tweets.empty?
    twits_filtered = apply_filters(tweets)

    twits_filtered.sort_by { |t| [t.followers_count, t.retweets_count, t.likes_count] }
  end

  def apply_filters(tweets)
    filtered_twits = @mention_user_filter.filter(tweets, '@locaweb')
    filtered_twits = @exclude_reply_to_filter.filter(filtered_twits, '42')

    filtered_twits
  end
end
