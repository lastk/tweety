class Tweet::ExcludeReplyToFilter
  def filter(tweets, user_id)
    tweets.select { |tweet| tweet.in_reply_to_user_id != user_id }
  end
end
