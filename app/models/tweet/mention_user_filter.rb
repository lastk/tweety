class Tweet::MentionUserFilter
  def filter(tweets, user)
    tweets.select { |tweet| tweet.text =~ /#{user}/  }
  end
end
