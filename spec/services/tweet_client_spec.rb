require 'rails_helper'
describe TweetClient do
  context 'listing tweets' do
    it 'lists tweets from locaweb repository' do
      client = TweetClient.new(LocawebTweets.new)
      result = client.query({})

      expect(result).not_to be_empty
      first_result = result.first

      expect(first_result).to be_an Tweet
    end
  end
end
