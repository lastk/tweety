class LocawebTweets
  def search_tweets(query)
    response = connection.get do |req|
      req.url "/tweeps"
      req.headers['Username'] = 'rafael.leite.oliveira@gmail.com'
    end
    handle_response(response)
  end

  private

  def connection
    @conn ||= Faraday.new(url: 'http://tweeps.locaweb.com.br') do |faraday|
      faraday.request  :url_encoded             # form-encode POST params
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end
  end

  def handle_response(response)
    #TODO take care of possible http errors after!
    if response.status == 200
      JSON.parse(response.body)
    end
  end
end
