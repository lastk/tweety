require 'rails_helper'

describe TweetPresenter do
  context 'presenting objects' do
    it 'delegates methods to the object' do
      tweet =  Tweet.new(screen_name: 'rafael', followers_count: 10,
                         text: 'Hellooo @locaweb, wazup?')
      presented = TweetPresenter.new(tweet)

      expect(presented.screen_name).to eq(tweet.screen_name)
      expect(presented.followers_count).to eq(tweet.followers_count)
      expect(presented.text).to eq(tweet.text)
    end

    it 'returns the link to twitter' do
      tweet =  Tweet.new(screen_name: 'rafael', followers_count: 10,
                        text: 'Hellooo @locaweb, wazup?', profile_image: '/image.jpg')
      profile_link = "<a href=\"http://twitter.com/rafael\"><img src=\"/image.jpg\" alt=\"Image\" /></a>"

      presented = TweetPresenter.new(tweet)

      expect(presented.profile_link).to eq(profile_link)
    end

    it 'returns the datetime formated' do
      now = Time.now
      tweet = Tweet.new(id: 10, screen_name: 'rafael', created_at: now)
      expected_link = "<a href=\"http://twitter.com/rafael/10\">#{now.to_s(:post)}</a>"

      presented = TweetPresenter.new(tweet)

      expect(presented.tweet_link).to eq(expected_link)
    end
  end
end
