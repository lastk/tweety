# Configurando o projeto: 

* bundle install
* rails s

### visite a página: 'http://localhost:3000'

As regras são as seguintes:

* Buscar tweets que mencionem o usuário da Locaweb

* Excluir tweets que sejam replies para tweets da Locaweb


É preciso ordenar esses resultados para saber quais são mais urgentes. Para isso, devem ser usadas as seguintes regras:

* Usuários com mais seguidores

* Tweets que tenham mais retweets (considerar apenas o RT oficial do Twitter)

* Tweet com mais likes


Apresente os resultados com a ordenação acima e os campos:

* screen_name (@usuario) que fez o tweet (com link para o perfil)

* Número de seguidores do autor, número de retweets e likes do tweet

* Conteúdo do tweet

* Data e hora com link para o tweet
